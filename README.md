# browserless-webscraping

NOTES:

on JSDOM:
Google's fast xpath library can be integrated with JSDOM like this:
https://github.com/jsdom/jsdom/wiki/Using-Google-XPath-implementation-with-jsdom
But the newest version of JSDOM no longer supports this integration... need to look further into injecting scripts with JSDOM

on OSMOSIS;
There is allegedly support for XPATH queries in Osmosis but it is difficult to debug
