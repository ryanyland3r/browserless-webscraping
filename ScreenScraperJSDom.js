const jsdom = require("jsdom");
//const wgxpath = require('wgxpath');
// const jsdom = require("jsdom/lib/old-api.js");

jsdom.env({
    url: 'https://www.w3.org/TR/xpath/',
    scripts: [require('path').join(__dirname, 'wgxpath.install.js')],
    // or from Net: [`https://github.com/google/wicked-good-xpath/releases/download/1.3.0/wgxpath.install.js`],
    done: (err, window) => {
        window.wgxpath.install(window, true);
        console.log(window.document.evaluate('.//title', window.document.documentElement,
                null, window.XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.textContent);
    }
    });

// const { JSDOM } = jsdom;

// JSDOM.fromFile("/Users/isabelklee/Documents/workspace-ryan/browserless-webscraping/demo/chase-markup.html")
//     .then(dom => {
//     });

    