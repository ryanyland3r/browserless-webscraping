package com.example.demo;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

class DemoApplicationTests {

	@Test
	void test() throws Exception{
		//TODO make relative path
		System.setProperty("webdriver.chrome.driver", "/Users/isabelklee/Documents/workspace-ryan/browserless-webscraping/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("https://chase.com");
		//String pageSource = driver.getPageSource();
		String pageMarkup = ((JavascriptExecutor) driver).executeScript("return document.documentElement.outerHTML").toString();
		//FileUtils.writeStringToFile(new File("chase-rich.html"), pageSource);
		FileUtils.writeStringToFile(new File("chase-markup.html"), pageMarkup);

		
		driver.close();
	}

}